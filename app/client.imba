global css html,body background-color:#f3f3f3
css .container p:30px mt:30px mb:30px w:95% min-width:95% background-color:#fff border-radius:10px
css .frame border-color:#e6e6e6 border-style:solid border-width:2px p:20px border-radius:5px
css .row mb:20px
css .logo max-width:100% max-height:100px
css .title-container ta:center margin:auto
css .gap-md mb:20px
css .gap-sm mb:10px
css .gap-lg mb:40px
css .navbar-style bc:#fff fs:20px

import { HttpClient } from "./core/http"
import { SalesAmountPerSalesPersonChart, NumberOfSalesPerSalesPersonChart, SalesPeopleContributionChart, TurnoverChart, OrderCountChart, AverageSellingPriceChart, MedianSellingPriceChart, QuotationsStatusProportionChart, DistributionAreaChart } from "./components"

let currentYear = 2015
let currentSalesperson = "Y. Weissgerber"

tag app
	def render
		const httpClient = new HttpClient("https://api.eltaco.ovh")
		const report = 
			annualReports: await (await httpClient.get "/annual-reports").json!
			annualQuotationReports: await (await httpClient.get "/annual-quotation-reports").json!
			salesPeopleReports: await (await httpClient.get "/sales-people-reports").json!
			distributionAreaReports: await (await httpClient.get "/distribution-area-reports").json!

		report.salesPeopleReports = report.salesPeopleReports.filter(do(x) x.salesPerson.value !== "Divers" && x.salesPerson.value !== "Service")
		
		let updateCurrentYear = do(d)
			currentYear = d
			imba.commit!

		let updateCurrentSalesperson = do(d)
			currentSalesperson = d
			imba.commit!

		<self>
			<div.container>
				<div.row.gap-lg>
					<div.col-3>
						<img.logo src="../assets/images/logo-alfabureau.png">
					<div.col-6.title-container>
						<h1> "Tableau de bord"
					<div.col-3[ta:right]>
				<div.row.gap-md.frame>
					<div.col-12.gap-sm>
						<TurnoverChart report=report.annualReports>
					<div.col-4>
						<OrderCountChart report=report.annualReports>
					<div.col-4>
						<AverageSellingPriceChart report=report.annualReports>
					<div.col-4>
						<MedianSellingPriceChart report=report.annualReports>

				<div.row.frame.gap-md>
					<div>
						<div.title-container>
							<h3> "Choix de l'année"
						<nav.navbar.navbar-expand-lg.navbar-light.navbar-style>
							<div#navbarSupportedContent.collapse.navbar-collapse>
								<ul.navbar-nav.me-auto.mb-2.mb-lg-0[m:auto]>
									for d,i in report.annualReports
										<li.nav-item>
											<a.nav-link.active @click=(updateCurrentYear(d.year.value)) route-to="/{d.year.value}"> d.year.value
					<hr>
					<div.gap-sm>
					<div.col-12.col-xl-8>
						<div.row>
							<SalesAmountPerSalesPersonChart route="/:year" report=report.salesPeopleReports>
							<SalesAmountPerSalesPersonChart route="/$" report=report.salesPeopleReports>
						<div.row>
							<NumberOfSalesPerSalesPersonChart route="/:year" report=report.salesPeopleReports>
							<NumberOfSalesPerSalesPersonChart route="/$" report=report.salesPeopleReports>
					<div.col-12.col-xl-4>
						<SalesPeopleContributionChart route="/:year" report=report.salesPeopleReports>
						<SalesPeopleContributionChart route="/$" report=report.salesPeopleReports>


					<div.row.gap-md>
						<div>
						<div.title-container>
							<h3> "Choix du commercial"
						<nav.navbar.navbar-expand-lg.navbar-light.navbar-style>
							<div#navbarSupportedContent.collapse.navbar-collapse>
								<ul.navbar-nav.me-auto.mb-2.mb-lg-0[m:auto]>
									for d,i in report.salesPeopleReports.filter(do(x) x.year.value === currentYear)
										<li.nav-item>
											<a.nav-link.active @click=(updateCurrentSalesperson(d.salesPerson.value)) route-to="/{currentYear}/{d.salesPerson.value}"> d.salesPerson.value
						<hr>
						<div.col-12[h:600px]>
							<QuotationsStatusProportionChart route="/:year/:salesPerson$" report=report.annualQuotationReports>
							<QuotationsStatusProportionChart route="/$" report=report.annualQuotationReports>

						<div.col-12[h:800px]>
							<DistributionAreaChart route="/:year/:salesPerson$" report=report.distributionAreaReports>
							<DistributionAreaChart route="/$" report=report.distributionAreaReports>

imba.mount <app>
