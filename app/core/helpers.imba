const decodeUriComponent = require('decode-uri-component');

def groupBy(array, criteria)
	const reduceFun = do(acc, currentValue)
		const groupIndex = acc.findIndex(do(e) e[0][criteria] === currentValue[criteria])
		if groupIndex === -1
			acc.push([currentValue])
		else 
			acc[groupIndex].push(currentValue)
		acc
	array.reduce(reduceFun, [])

def distinct(array)
	array.filter(do(value, index, current) current.indexOf(value) === index)

def decodeUri(uri)
	decodeUriComponent(uri)

export { groupBy, distinct, decodeUri }
