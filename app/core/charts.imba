class Chart
	constructor chartStructure, container
		this.chartStructure = chartStructure
		this.container = container

	def fill data\any
		this.chartStructure.series = data
	
	def display
		Highcharts.setOptions(
			plotOptions:
				series:
					animation: false
		)
		Highcharts.chart(this.container, this.chartStructure)

class MapChart
	constructor chartStructure, container
		this.chartStructure = chartStructure
		this.container = container

	def fill data\any
		this.chartStructure.series = data
	
	def display
		Highcharts.mapChart(this.container, this.chartStructure)
	
class ColumnChart
	constructor container, specs\any
		let chartStructure =
			chart:
				type: 'column'
			title:
				text: specs.title
			subtitle:
				text: (specs.subtitle == undefined ? null : specs.subtitle)
			xAxis:
				categories: specs.categories
				crosshair: true
			yAxis:
				min: 0
				title:
					text: (specs.measurementTitle == undefined ? null : specs.measurementTitle)
			tooltip:
				valueSuffix: (specs.tooltipValueSuffix == undefined ? "" : specs.tooltipValueSuffix)
				shared: true
				useHTML: true
			plotOptions:
				column:
					pointPadding: 0.2
					borderWidth: 0
			series: []

		this.chart = new Chart(chartStructure, container)

	def fill data\any
		this.chart.fill(data)
	
	def display
		this.chart.display()

class BarChart
	constructor container, specs\any
		let chartStructure = 
			chart:
				type: "bar"
			title:
				text: specs.title
			subtitle:
				text: (specs.subtitle == undefined ? null : specs.subtitle)
			xAxis:
				categories: specs.categories
				title: 
					text: null
			yAxis:
				min: 0
				title:
					text: specs.measurementTitle
					align: "high"
				labels:
					overflow: "justify"
			tooltip:
				valueSuffix: (specs.tooltipValueSuffix == undefined ? "" : specs.tooltipValueSuffix)
			plotOptions:
				bar:
					dataLabels:
						enabled: true
			credits:
				enabled: false
			series: []

		this.chart = new Chart(chartStructure, container)

	def fill data\any
		this.chart.fill(data)

	def display
		this.chart.display()

class StackedBarChart
	constructor container, specs\any
		let chartStructure = 
			chart:
				type: "bar"
			title:
				text: specs.title
			subtitle:
				text: (specs.subtitle == undefined ? null : specs.subtitle)
			xAxis:
				categories: specs.categories
				title: 
					text: null
			yAxis:
				min: 0
				title:
					text: specs.measurementTitle
					align: "high"
				labels:
					overflow: "justify"
			tooltip:
				valueSuffix: (specs.tooltipValueSuffix == undefined ? "" : specs.tooltipValueSuffix)
			plotOptions:
				bar:
					dataLabels:
						enabled: true
				series:
					stacking: "normal"
			credits:
				enabled: false
			series: []

		this.chart = new Chart(chartStructure, container)

	def fill data\any
		this.chart.fill(data)

	def display
		this.chart.display()

class PieChart
	constructor container, specs\any
		let chartStructure = 
			chart:
				plotBackgroundColor: null
				plotBorderWidth: null
				plotShadow: false
				type: "pie"
			title:
				text: specs.title
			tooltip:
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			accessibility:
				point:
					valueSuffix: "%"
			plotOptions:
				pie:
					allowPointSelect: true
					cursor: "pointer"
					dataLabels:
						enabled: true
						format: '<b>{point.name}</b>: {point.percentage:.1f} %'
			series: []

		this.chart = new Chart(chartStructure, container)

	def fill data\any
		this.chart.fill(data)

	def display
		this.chart.display()

class StackedPercentageColumnChart
	constructor container, specs\any
		let chartStructure = 
			chart:
				type: "column"
			title:
				text: specs.title
			xAxis:
				categories: specs.categories
			yAxis:
				min: 0
				title:
					text: specs.measurementTitle
			tooltip:
				pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>'
				shared: true
			plotOptions:
				column:
					stacking: "percent"
			series: []

		this.chart = new Chart(chartStructure, container)

	def fill data\any
		this.chart.fill(data)

	def display
		this.chart.display()

class CategoryMapChart
	constructor container, specs\any
		let chartStructure = 
			chart:
				map: "custom/world"
			colors: [
				"rgba(0,0,25,0.2)"
				"rgba(0,0,25,0.4)"
				"rgba(0,0,25,0.7)"
				"rgba(0,0,25,1)"
			]
			title:
				text: specs.title
			mapNavigation:
				enabled: true
			legend:
				title:
					text: specs.measurementTitle
				align: "left"
				verticalAlign: "bottom"
				floating: true
				layout: "vertical"
				valueDecimals: 0
				symbolRadius: 0
				symbolHeight: 14
			colorAxis:
				dataClasses: specs.dataClasses
			plotOptions:
				map:
					color: "#000025"
			series: []

		this.chart = new MapChart(chartStructure, container)

	def fill data\any
		this.chart.fill(data)

	def display
		this.chart.display()

export { ColumnChart, BarChart, StackedBarChart, PieChart, StackedPercentageColumnChart, CategoryMapChart }
