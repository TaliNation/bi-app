import { BarChart, StackedBarChart, PieChart, ColumnChart, StackedPercentageColumnChart, CategoryMapChart } from "./core/charts"
import { groupBy, distinct, decodeUri } from "./core/helpers"

const DEFAULT_YEAR = 2015
const DEFAULT_SALESPERSON = "Y. Weissgerber"

tag SalesAmountPerSalesPersonChart
	prop year\number
	prop report\any

	def routed params, state
		year = (params.year == undefined ? DEFAULT_YEAR : parseInt(params.year))
		
	def render
		<self>
			<div$container[h:100% max-height:100%]>

	def rendered
		let filteredReport = report
			.filter(do(d) d.year.value === year)
			.map(do(d) 
				year: d.year.value
				salesPerson: d.salesPerson.value
				salesTotal: (d.salesTotal === null ? 0 : d.salesTotal.value)
				salesTotalGoal: (d.salesTotalGoal === null ? 0 : d.salesTotalGoal.value)
			)

		let categories = filteredReport.map(do(d) d.salesPerson)

		let chart = new BarChart($container, 
			title: "Montant des ventes par commerciaux"
			categories: categories
			measurementTitle: "Montant des ventes"
			tooltipValueSuffix: " €"
		)

		let data = [
			{
				name: "Montant des ventes"
				data: filteredReport.map(do(d) d.salesTotal)
			}
			{
				name: "Objectif"
				data: filteredReport.map(do(d) d.salesTotalGoal)
			}
		]

		chart.fill(data)
		chart.display()

tag NumberOfSalesPerSalesPersonChart
	prop year\number
	prop report\any

	def routed params, state
		year = (params.year == undefined ? DEFAULT_YEAR : parseInt(params.year))

	def render
		<self>
			<div$container[h:100% max-height:100%]>

	def rendered
		let filteredReport = report
			.filter(do(d) d.year.value === year)
			.map(do(d) 
				year: d.year.value
				salesPerson: d.salesPerson.value
				salesAmount: (d.salesAmount === null ? 0 : d.salesAmount.value)
				quoteAmount: (d.quoteAmount === null ? 0 : d.quoteAmount.value)
			)
		let categories = filteredReport.map(do(d) d.salesPerson)

		let chart = new StackedBarChart($container, 
			title: "Nombre de ventes par commerciaux"
			categories: categories
			measurementTitle: "Nombre de ventes"
		)

		let data = [
			{
				name: "Devis - Ventes"
				data: filteredReport.map(do(d) d.quoteAmount - d.salesAmount)
			}
			{
				name: "Nombre de ventes"
				data: filteredReport.map(do(d) d.salesAmount)
			}
		]

		chart.fill(data)
		chart.display()


tag SalesPeopleContributionChart
	prop year\number
	prop report\any

	def routed params, state
		year = (params.year == undefined ? DEFAULT_YEAR : parseInt(params.year))

	def render
		<self>
			<div$container[h:100% max-height:100%]>
	
	def rendered
		let filteredReport = report
			.filter(do(d) d.year.value === year)
			.map(do(d) 
				year: d.year.value
				salesPerson: d.salesPerson.value
				salesTotal: (d.salesTotal === null ? 0 : d.salesTotal.value)
			)

		let chart = new PieChart($container, 
			title: "Contribution des commerciaux au chiffre d'affaire"
		)

		let data = [
			name: "Contribution"
			colorByPoint: true
			data: filteredReport.map(do(d) 
				name: d.salesPerson
				y: d.salesTotal
			)
		]

		chart.fill(data)
		chart.display()

tag TurnoverChart
	prop report\any

	def render
		<self>
			<div$container>
	
	def rendered
		let filteredReport = report
			.map(do(d) 
				year: d.year.value
				turnover: (d.turnover === null ? 0 : d.turnover.value)
				turnoverGoal: (d.turnoverGoal === null ? 0 : d.turnoverGoal.value)
			)
		let categories = filteredReport.map(do(d) d.year)

		let chart = new ColumnChart($container,
			title: "Chiffre d'affaire"
			categories: categories
			measurementTitle: "Montant des ventes"
			tooltipValueSuffix: ' €'
		)

		let data = [
			{
				name: "Objectif"
				data: filteredReport.map(do(d) d.turnoverGoal)
				pointPadding: 0
				pointPlacement: 0.2
			}
			{
				name: "Chiffre d'affaire"
				data: filteredReport.map(do(d) d.turnover)
				pointPadding: 0
				pointPlacement: -0.2
			}
		]

		chart.fill(data)
		chart.display()

tag MedianSellingPriceChart
	prop report\any

	def render
		<self>
			<div$container>
	
	def rendered
		let filteredReport = report
			.map(do(d) 
				year: d.year.value
				medianSellingPrice: (d.medianSellingPrice === null ? 0 : d.medianSellingPrice.value)
			)
		let categories = filteredReport.map(do(d) d.year)

		let chart = new ColumnChart($container,
			title: "Prix de vente médian"
			categories: categories
			measurementTitle: "Montant des ventes"
			tooltipValueSuffix: ' €'
		)

		let data = [
			{
				name: "Prix de vente médian"
				data: filteredReport.map(do(d) d.medianSellingPrice)
			}
		]

		chart.fill(data)
		chart.display()

tag AverageSellingPriceChart
	prop report\any

	def render
		<self>
			<div$container>
	
	def rendered
		let filteredReport = report
			.map(do(d) 
				year: d.year.value
				averageSellingPrice: (d.averageSellingPrice === null ? 0 : d.averageSellingPrice.value)
			)
		let categories = filteredReport.map(do(d) d.year)

		let chart = new ColumnChart($container,
			title: "Prix de vente moyen"
			categories: categories
			measurementTitle: "Montant des ventes"
			tooltipValueSuffix: ' €'
		)

		let data = [
			{
				name: "Prix de vente moyen"
				data: filteredReport.map(do(d) d.averageSellingPrice)
			}
		]

		chart.fill(data)
		chart.display()

tag OrderCountChart
	prop report\any

	def render
		<self>
			<div$container>
	
	def rendered
		let filteredReport = report.map(do(d) 
			year: d.year.value
			orderAmount: (d.orderAmount === null ? 0 : d.orderAmount.value)
		)
		let categories = filteredReport.map(do(d) d.year)

		let chart = new ColumnChart($container,
			title: "Nombre de commandes"
			categories: categories
			measurementTitle: "Montant des ventes"
			tooltipValueSuffix: ''
		)

		let data = [
			name: "Nombre de commandes"
			data: filteredReport.map(do(d) d.orderAmount)
		]

		chart.fill(data)
		chart.display()

tag QuotationsStatusProportionChart
	prop salesPerson
	prop report\any

	def routed params, state
		salesPerson = (params.salesPerson == undefined ? DEFAULT_SALESPERSON : decodeUri(params.salesPerson))

	def render
		<self>
			<div$container[h:100% max-height:100%]>
	
	def rendered
		let filteredReport = report
			.filter(do(d) d.salesPerson.value === salesPerson)
			.map(do(d) 
				year: d.year.value
				quotationState: d.quotationState.value
				percentage: (d.percentage === null ? 0 : d.percentage.value)
			)
		let years = distinct(filteredReport.map(do(d) d.year))
		
		let chart = new StackedPercentageColumnChart($container,
			title: "Proportion de statut des devis"
			categories: years
			measurementTitle: "Statut des devis"
		)

		let quotationStatus = distinct(filteredReport.map(do(d) d.quotationState))

		let data = []
		for qs, qsi in quotationStatus
			data.push(
				name: qs
				data: []
			)
			for y, yi in years
				const row = filteredReport.find(do(d) d.year === y && d.quotationState === qs)
				if !row
					data[qsi].data.push(0)
				else
					data[qsi].data.push(row.percentage)
		
		chart.fill(data)
		chart.display()

tag DistributionAreaChart
	prop year
	prop salesPerson
	prop report\any

	def routed params, state
		year = (params.year == undefined ? DEFAULT_YEAR : parseInt(params.year))
		salesPerson = (params.salesPerson == undefined ? DEFAULT_SALESPERSON : decodeUri(params.salesPerson))

	def render
		<self>
			<div$container[h:100% max-height:100%]>
	
	def rendered
		let filteredReport = report
			.filter(do(d) d.year.value === year && d.salesPerson.value === salesPerson)
			.map(do(d) 
				country: d.country.value
				salesTotal: d.salesTotal.value
			)
		
		let chart = new CategoryMapChart($container,
			title: "Zones de distribution"
			measurementTitle: "Chiffre d'affaire"
			dataClasses: [
				{
					from: 0
					to: 10000
				}
				{
					from: 10000
					to: 100000
				}
				{
					from: 100000
					to: 1000000
				}
				{
					from: 1000000
				}
			]
		)

		let data = [
			data: filteredReport.map(do(d)
				code: d.country
				value: d.salesTotal
				name: d.country
			)
			joinBy: ['iso-a2', 'code']
			name: "Chiffre d'affaire"
			states:
				hover:
					color: "#a4edba"
			tooltip:
				valueSuffix: "€"
			shadow: false
		]
		
		chart.fill(data)
		chart.display()



export { SalesAmountPerSalesPersonChart, NumberOfSalesPerSalesPersonChart, SalesPeopleContributionChart, TurnoverChart, OrderCountChart, AverageSellingPriceChart, MedianSellingPriceChart, QuotationsStatusProportionChart, DistributionAreaChart }
